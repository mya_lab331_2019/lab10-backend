package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.repository.LecturerRepository;
import java.util.List;


@Repository
@Profile("lecDao")
@Slf4j
public class LectureDaoImpl implements LecturerDao{
    @Autowired
    LecturerRepository lecturerRepository;


    @Override
    public List<Lecturer> getAllLecturer() {
        log.info("find all student in db");
        return lecturerRepository.findAll();
    }

    @Override
    public Lecturer findById(Long id) {
        log.info("find student from id {} from database", id);
        return lecturerRepository.findById(id).orElse(null);
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        log.info("save student to database");
        return lecturerRepository.save(lecturer);
    }
}