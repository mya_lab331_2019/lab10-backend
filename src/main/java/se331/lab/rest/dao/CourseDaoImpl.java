package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Course;
import se331.lab.rest.repository.CourseRepository;

import java.util.List;
@Repository
@Profile("coDao")
@Slf4j
public class CourseDaoImpl implements CourseDao{
    @Autowired
    CourseRepository courseRepository;

    @Override
    public List<Course> getAllCourse() {
        log.info("find all lecturer in db");
        return courseRepository.findAll();
    }

    @Override
    public Course findById(Long courseId) {
        log.info("find course from id {} from database", courseId);
        return courseRepository.findById(courseId).orElse(null);
    }

    @Override
    public Course saveCourse(Course course) {
        log.info("save Course to database");
        return courseRepository.save(course);
    }
}

