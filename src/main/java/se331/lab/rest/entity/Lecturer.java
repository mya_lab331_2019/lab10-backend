package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lecturer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    String surname;
    @OneToMany(mappedBy = "advisor")
    @Builder.Default
    @JsonManagedReference
    List<Student> advisees = new ArrayList<>();
    @OneToMany(mappedBy = "lecturer")
    @JsonManagedReference
    @Builder.Default
    List<Course> courses = new ArrayList<>();
}