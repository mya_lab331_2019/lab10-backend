package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String content;
    String courseId;
    String courseName;
    @ManyToOne
    @JsonBackReference
    Lecturer lecturer;
    @ManyToMany
    //@ManyToMany(mappedBy = "enrolledCourses")
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    @ToString.Exclude
    List<Student> students = new ArrayList<>();
}
