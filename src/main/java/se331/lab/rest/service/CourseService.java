package se331.lab.rest.service;

import se331.lab.rest.entity.Course;
import java.util.List;

public interface CourseService {
    List<Course> getAllCourse();
    Course findById(Long id);
    Course saveCourse(Course course);
}